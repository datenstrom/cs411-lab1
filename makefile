PROJECT=product_description
BIBTEX=bibtex
OUTDIR = public/
TEXDIR = .
INPUTDIR = sections/

# Pygment files
PFILE = $(PROJECT).pyg
OFILE = $(PROJECT).out.pyg

# The compiler and the -output-directory flag
TCC = pdflatex

# Mode options = nonstopmode, batchmode, scrollmode, errorstopmode
MODE = nonstopmode
TEXFLAGS = -interaction $(MODE) -halt-on-error -file-line-error -shell-escape

all: documentation clean

documentation:
	$(TCC) $(TEXFLAGS) $(TEXDIR)/$(PROJECT).tex 
	makeglossaries $(PROJECT)
	biber $(TEXDIR)/$(PROJECT)
	$(TCC) $(TEXFLAGS) $(TEXDIR)/$(PROJECT).tex 
	makeglossaries $(PROJECT)
	biber $(TEXDIR)/$(PROJECT)
	$(TCC) $(TEXFLAGS) $(TEXDIR)/$(PROJECT).tex 
	makeglossaries $(PROJECT)
	biber $(TEXDIR)/$(PROJECT)
	$(TCC) $(TEXFLAGS) $(TEXDIR)/$(PROJECT).tex 

clean:
	touch $(PROJECT).pdf
	mv $(PROJECT).pdf $(OUTDIR)
	rm -f $(PROJECT).aux
	rm -f $(PROJECT).toc
	rm -f $(PROJECT).log
	rm -f $(PROJECT).glo
	rm -f $(PROJECT).glg
	rm -f $(PROJECT).idx
	rm -f $(PROJECT).ist
	rm -f $(PROJECT).lof
	rm -f $(PROJECT).fff
	rm -f $(PROJECT).bcf
	rm -f $(PROJECT).log
	rm -f $(PROJECT).out
	rm -f $(PROJECT).run.xml
	rm -f $(PROJECT).pyg
	rm -f $(PROJECT).bbl
	rm -f $(PROJECT).blg
	rm -f $(PROJECT).gls
	rm -f $(PROJECT).xdy
	rm -f $(INPUTDIR)*.aux
	rm -f $(INPUTDIR)*.bcf
	rm -f $(INPUTDIR)*.log
	rm -f $(INPUTDIR)*.out
	rm -f $(INPUTDIR)*.run.xml
	rm -f $(INPUTDIR)*.pyg
	rm -f $(INPUTDIR)*.swp
	rm -f glossary.aux
